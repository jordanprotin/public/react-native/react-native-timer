import React, {
  useCallback, 
  useEffect,
  useState
} from 'react';

import {
  ActivityIndicator,
  Button,
  Easing,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View
} from 'react-native';

import { Colors } from 'react-native/Libraries/NewAppScreen';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { useTimer } from 'react-use-precision-timer';

// =================================================================================

const MAX = 20;

const INITIAL_STATE = {
  isStartingTimer: false,
  progress: 0,
  fill: 0,
  loading: false
};

// =================================================================================

const App: () => React$Node = () => {

  /**
   * LOCAL STATE
   */
  const [isStartingTimer, setIsStartingTimer] = useState(INITIAL_STATE.isStartingTimer);
  const [progress, setProgress] = useState(INITIAL_STATE.progress);
  const [fill, setFill] = useState(INITIAL_STATE.fill);
  const [loading, setLoading] = useState(INITIAL_STATE.loading);

  /**
   * We use https://github.com/devboldly/react-use-precision-timer for timers.
   */
  const timer = useTimer({ 
    delay: 1000, // fires once per second
    callback: () => setProgress(progress + 1)
  });

  /**
   * Start or stop the timer!
   */
  const onPressTimer = useCallback(() => {
    setLoading(!loading);
    setIsStartingTimer(!isStartingTimer);
  }, [isStartingTimer]);

  /**
   * Side-effect called in order to start or stop the timer.
   */
  useEffect(() => {
    if (isStartingTimer) { // begin
      timer.start();
    } else { // we stop the timer!
      resetValues();
      stopTimer();
    }
  }, [isStartingTimer]);

  /**
   * This function allows to clean all values which have been used.
   */
  const resetValues = useCallback(() => {
    setProgress(INITIAL_STATE.progress);
    setFill(INITIAL_STATE.fill);
    setLoading(INITIAL_STATE.loading);
  }, []);

  /**
   * We calculate the fill.
   */
  useEffect(() => {
    if (progress > 0) {
      const value = (progress/MAX) * 100;
      
      if (value > 100) { // we finished!
        setIsStartingTimer(INITIAL_STATE.isStartingTimer);

        // we stop the timer!
        stopTimer();
        resetValues();
      } else { // continue the progress
        setFill(value);
      }
    }
  }, [progress]);

  /**
   * Side-effect called to manage the showing of the activity indicator.
   */
  useEffect(() => {
    fill > 0 
      ? setLoading(INITIAL_STATE.loading)
      : setLoading(true);
  }, [fill]);

  /**
   * This is a shortcut to stop the active timer.
   */
  const stopTimer = useCallback(() => {
    timer.stop();
  }, [timer]);

  // --
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.scrollView}>
          <View style={styles.header}>
            <Text style={styles.title}>RN timer demo</Text>
          </View>

          <View style={styles.body}>
            {isStartingTimer && (
              <View style={styles.shapeContainer}>
                {loading ? (
                  <ActivityIndicator size="large" color="#ffffff" />
                ) : (
                  <AnimatedCircularProgress
                    size={200}
                    width={3}
                    backgroundWidth={30}
                    fill={fill}
                    tintColor="#00e0ff"
                    backgroundColor="#3d5875"
                    rotation={360}
                    duration={1000}
                    easing={Easing.linear}
                  >
                    {fillValue => <Text style={styles.progressText}>{Math.round((MAX * fillValue) / 100)}</Text>}
                  </AnimatedCircularProgress>
                )}
              </View>
            )}

            <View style={styles.buttonContainer}>
              <Button 
                onPress={onPressTimer} 
                title={`${isStartingTimer ? 'Stop' : 'Start'} timer`}
                color="#841584" 
                accessibilityLabel="Timer button" 
              />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  header: {
    paddingBottom: 40,
    paddingTop: 96,
    paddingHorizontal: 32,
    backgroundColor: Colors.lighter,
  },
  title: {
    fontSize: 40,
    fontWeight: '600',
    textAlign: 'center',
    color: Colors.black,
  },
  body: {
    backgroundColor: Colors.white,
  },
  shapeContainer: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#152d44',
    padding: 50,
  },
  buttonContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  progressText: {
    textAlign: 'center',
    color: '#7591af',
    fontSize: 50,
    fontWeight: '100',
  },
});

export default App;
