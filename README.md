> Auteur : Jordan Protin | jordan.protin@yahoo.com

# React Native - Timer

Il s'agit d'un timer représenté sous la forme d'un cercle progressif.

> Réalisé à l'aide de [React Native Circular Progress](https://github.com/bartgryszko/react-native-circular-progress) et [React Native Precision Timer](https://github.com/devboldly/react-use-precision-timer).

## Installation

```bash
$ git clone https://gitlab.com/jordanprotin/public/react-native/react-native-timer.git
$ cd react-native-timer
$ npm install
```

## Lancement

```bash
$ cd rreact-native-timer
$ npx react-native start
$ npx react-native run-android
```
